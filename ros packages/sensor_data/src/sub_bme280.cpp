#include <ros/ros.h>
#include "sensor_data/BME280.h"//custom BME280 message
#include <iostream>
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
#include "unistd.h"

class Bme280Sub {
public:

ros::NodeHandle n;
ros::Subscriber sub_bme280;
sensor_data::BME280 bme280_msg;

std::string bme280_topic;

float temp,pressure,altitude;

Bme280Sub() {
  n = ros::NodeHandle("~");
  sub_bme280 = n.subscribe("/bme280",10, &Bme280Sub::bme280Callback, this);
  ROS_INFO("Initializing node .................................");
} //Constuctor

void bme280Callback(const sensor_data::BME280::ConstPtr &bme280_msg);
};


void Bme280Sub::bme280Callback(const sensor_data::BME280::ConstPtr &bme280_msg) {
temp = bme280_msg->temperature;
pressure = bme280_msg->pressure;
altitude = bme280_msg->altitude;
ROS_INFO("BME280 readings: temperature- %f, pressure - %f, altitude - %f",temp, pressure, altitude);
}



int main(int argc, char** argv) {

    ros::init(argc, argv, "bme280_sub");
    Bme280Sub obj;
    ros::spin();
    return 0;
}

