#! /usr/bin/env python3
import time
import board
import adafruit_bmp280
import rospy

from sensor_data.msg import BME280

class SensorPub():

    def __init__(self):
        self.pub3 = rospy.Publisher('/bme280', BME280, queue_size=1)
        self.i2c = board.I2C()  # uses board.SCL and board.SDA
        self.bmp280 = adafruit_bmp280.Adafruit_BMP280_I2C(self.i2c,address=0x76)
        self.barometer_msg = BME280()
        self.bmp280.sea_level_pressure = 1013.25


    def pub_bme280(self):
       temp = self.bmp280.temperature
       pressure = self.bmp280.pressure
       altitude = self.bmp280.altitude
       self.barometer_msg.header.stamp = rospy.Time.now()
       self.barometer_msg.temperature = temp
       self.barometer_msg.pressure = pressure
       self.barometer_msg.altitude = altitude
       self.pub3.publish(self.barometer_msg)
       rospy.loginfo("Publishing into /bme280 topic!")

if __name__ == '__main__':
    rospy.init_node('bme280_publisher')
    obj = SensorPub()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        obj.pub_bme280()
        rate.sleep()

