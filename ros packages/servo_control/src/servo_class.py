#!/usr/bin/python3
import time
import rospy
import RPi.GPIO as GPIO
import pigpio

from servo_control.msg import ServoAngles
from servo_control.msg import ServoPWM
class ServoClass():

    def __init__(self):
     # Publishers & Subscribers Definition 
        self.pub_servo_pwm = rospy.Publisher('/servo_pwm', ServoPWM, queue_size=1)
        self.sub_servo_angles = rospy.Subscriber('/servo_angles', ServoAngles, self.servo_angles_callback)
     # Messages Definition 
        self.servo_pwm_msg = ServoPWM()
        self.servo_angles_msg = ServoAngles()
     # Rate
        self.rate = rospy.Rate(10) # 10hz
     # Shutdown handlers             
        self.ctrl_c = False
        rospy.on_shutdown(self.shutdownhook)
     # Other Classes' Variables           
        self.pi = pigpio.pi()
     # Custom Variables
        self.Servo1 = 18 #RPI 3B+ PWM PINS
        self.Servo2 = 12
        self.Servo3 = 19
        self.Servo4 = 13
        self.MAX_PWM = 2500 #in microseconds
        self.MIN_PWM = 500 #in microseconds
        self.MAX_ANGLE = 30 # in degrees
        self.MIN_ANGLE = -30 # in degrees
     
        """
        data.angle1 -> Pin 18
        data.angle2 -> Pin 12
        data.angle3 -> Pin 19
        data.angle4 -> Pin 13
        """
           
    def servo_angles_callback(self, data):
        if not self.pi.connected:
            rospy.logerror('pigpio not connected')
        elif data.angle1 > self.MAX_ANGLE or data.angle1 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')    
        elif data.angle2 > self.MAX_ANGLE or data.angle2 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range') 
        elif data.angle3 > self.MAX_ANGLE or data.angle3 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')  
        elif data.angle4 > self.MAX_ANGLE or data.angle4 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')                  
        else:        
            y1 = self.MIN_PWM
            y2 = self.MAX_PWM
            x1 = self.MIN_ANGLE
            x2 = self.MAX_ANGLE
            pwm1 = (y2-y1)*(data.angle1 - x1)/(x2-x1) + y1
            pwm2 = (y2-y1)*(data.angle2 - x1)/(x2-x1) + y1
            pwm3 = (y2-y1)*(data.angle3 - x1)/(x2-x1) + y1
            pwm4 = (y2-y1)*(data.angle4 - x1)/(x2-x1) + y1
            self.pi.set_servo_pulsewidth(self.Servo1, pwm1)
            self.pi.set_servo_pulsewidth(self.Servo2, pwm2)
            self.pi.set_servo_pulsewidth(self.Servo3, pwm3)
            self.pi.set_servo_pulsewidth(self.Servo4, pwm4)
            self.servo_pwm_msg.pulsewidth1 = pwm1
            self.servo_pwm_msg.pulsewidth1 = pwm2
            self.servo_pwm_msg.pulsewidth1 = pwm3
            self.servo_pwm_msg.pulsewidth1 = pwm4
            self.publish_const_servo_pulsewidth()     
            rospy.loginfo("Publishing into /servo_pwm topic!")
            
    def publish_once_servo_pulsewidth(self):
        """
        This is because publishing in topics sometimes fails the first time you publish.
        In continuous publishing systems, this is no big deal, but in systems that publish only
        once, it IS very important.
        """
        while not self.ctrl_c:
            connections = self.pub_servo_pwm.get_num_connections()
            if connections > 0:
                self.pub_servo_pwm.publish(self.servo_pwm_msg)
                rospy.loginfo("Pulsewidth Published")
                break
            else:
                self.rate.sleep()
     
    def publish_const_servo_pulsewidth(self):
        while not self.ctrl_c:
                self.pub_servo_pwm.publish(self.servo_pwm_msg)
                rospy.loginfo("Publishing Pulsewidth")
                self.rate.sleep()     
        
    def shutdownhook(self):
        # works better than the rospy.is_shutdown()
        self.ctrl_c = True


    def stop_all_servos(self):
       rospy.loginfo('Stopping all of servos...')
       self.pi.set_servo_pulsewidth(Servo1, 0) 
       self.pi.set_servo_pulsewidth(Servo2, 0) 
       self.pi.set_servo_pulsewidth(Servo3, 0) 
       self.pi.set_servo_pulsewidth(Servo4, 0) 
       
    def stop_one_servo(self, servo = 1):
       if servo == 18 or 1:
           self.pi.set_servo_pulsewidth(Servo1, 0) 
       elif servo == 12 or 2:
           self.pi.set_servo_pulsewidth(Servo1, 0)        
       elif servo == 18 or 3:
           self.pi.set_servo_pulsewidth(Servo1, 0) 
       elif servo == 12 or 4:
           self.pi.set_servo_pulsewidth(Servo1, 0)          
       else:     
           rospy.loginfo("Servo is incorrectly defined")
       rospy.loginfo("Stopping servo %d", servo)
       
if __name__ == '__main__':
    rospy.init_node('servo_node', anonymous=True)
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    try:
        ServoClass()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
        
        
