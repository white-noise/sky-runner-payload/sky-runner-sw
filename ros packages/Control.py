#! /usr/bin/env python3
import time
import board
import adafruit_fxos8700
import adafruit_fxas21002c
import rospy
import numpy as np
import RPi.GPIO as GPIO
import pigpio

from control.msg import ServoAngles
from control.msg import ServoPWM
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

class SensorPub():

    def __init__(self):
    # Publishers & Subscribers Definition 
        self.pub_imu_raw = rospy.Publisher('/imu/sensor_data_raw', Imu, queue_size=1)
        self.pub_mag_raw = rospy.Publisher('/imu/sensor_mag', MagneticField, queue_size=1)
        self.pub_imu_calib = rospy.Publisher('/imu/data_raw', Imu, queue_size=1)
        self.pub_mag_calib = rospy.Publisher('/imu/mag', MagneticField, queue_size=1)
        self.pub_servo_pwm = rospy.Publisher('/servo_pwm', ServoPWM, queue_size=1)
        self.sub_servo_angles = rospy.Subscriber('/servo_angles', ServoAngles, self.servo_angles_callback)   
     # Messages Definition        
        self.imu_msg = Imu()
        self.magnetometer_msg = MagneticField()
        self.servo_pwm_msg = ServoPWM()
        self.servo_angles_msg = ServoAngles()
     # Rate
        self.rate = rospy.Rate(10) # 10hz
     # Shutdown handlers             
        self.ctrl_c = False
        rospy.on_shutdown(self.shutdownhook)
     # Other Classes' Variables           
        self.pi = pigpio.pi()
        self.i2c = board.I2C() 
        self.fxas = adafruit_fxas21002c.FXAS21002C(self.i2c)
        self.fxos  = adafruit_fxos8700.FXOS8700(self.i2c)
     # Custom Variables
        self.Servo1 = 18 #RPI 3B+ PWM PINS
        self.Servo2 = 12
        self.Servo3 = 19
        self.Servo4 = 13
        self.MAX_PWM = 2500 #in microseconds
        self.MIN_PWM = 500 #in microseconds
        self.MAX_ANGLE = 30 # in degrees
        self.MIN_ANGLE = -30 # in degrees
        """
        data.angle1 -> Pin 18
        data.angle2 -> Pin 12
        data.angle3 -> Pin 19
        data.angle4 -> Pin 13
        """
        
        
					"""
				IMU DATA HANDLING
					"""        
        
    def pub_acc_gyro(self):
        accel_x, accel_y, accel_z = self.fxos.accelerometer
        gyro_x,  gyro_y,  gyro_z = self.fxas.gyroscope
        self.imu_msg.header.frame_id = "base_link"
        self.imu_msg.header.stamp = rospy.Time.now()
        self.imu_msg.linear_acceleration.x = accel_x
        self.imu_msg.linear_acceleration.y = accel_y
        self.imu_msg.linear_acceleration.z = accel_z
        self.imu_msg.angular_velocity.x = gyro_x
        self.imu_msg.angular_velocity.y = gyro_y
        self.imu_msg.angular_velocity.z = gyro_z
        self.pub_imu_raw.publish(self.imu_msg)
        rospy.loginfo("Publishing into /imu/data_raw topic!")

    def pub_magnetometer(self):
        mag_x, mag_y, mag_z = self.fxos.magnetometer
        self.magnetometer_msg.header.stamp = rospy.Time.now()
        self.magnetometer_msg.magnetic_field.x = mag_x
        self.magnetometer_msg.magnetic_field.y = mag_y
        self.magnetometer_msg.magnetic_field.z = mag_z
        self.pub_mag_raw.publish(self.magnetometer_msg)
        rospy.loginfo("Publishing into /mag/data_raw topic!")

    def calibrated_mag_data(self):
        mag_x, mag_y, mag_z = self.fxos.magnetometer
        raw_mag = np.array([[mag_x], [mag_y], [mag_z]])
        biases = np.array(np.mat('-13.525692; -35.817735; -31.867910'))
        A_inv = np.array([[1.099265, -0.033624, -0.005563], [-0.033624, 1.086130, 0.028439], [-0.005563, 0.028439, 1.156762]]);
        unbiased_mag = np.subtract(raw_mag, biases)
        calibrated_mag_data = np.matmul(A_inv, unbiased_mag)
        self.magnetometer_msg.header.stamp = rospy.Time.now()
        self.magnetometer_msg.magnetic_field.x = float(calibrated_mag_data[0])
        self.magnetometer_msg.magnetic_field.y = float(calibrated_mag_data[1])
        self.magnetometer_msg.magnetic_field.z = float(calibrated_mag_data[2])
        self.pub_mag_calib.publish(self.magnetometer_msg)
        rospy.loginfo("Publishing into /mag/calib_data topic!")
    
    def calibrated_imu_data(self):
        accel_x, accel_y, accel_z = self.fxos.accelerometer
        gyro_x,  gyro_y,  gyro_z = self.fxas.gyroscope
        raw_acc = np.array([[accel_x], [accel_y], [accel_z]])
        biases_acc = np.array(np.mat('-0.005820; -0.013673; -0.004559'))
        A_inv_acc = np.array([[1.007482, 0.002221, 0.000726], [0.002221, 1.027824, -0.004322], [0.000726, -0.004322, 0.976788]]);
        unbiased_acc = np.subtract(raw_acc, biases_acc)
        calibrated_imu_data = np.matmul(A_inv_acc, unbiased_acc)
        self.imu_msg.header.frame_id = "base_link"
        self.imu_msg.header.stamp = rospy.Time.now()
        self.imu_msg.linear_acceleration.x = float(calibrated_imu_data[0])
        self.imu_msg.linear_acceleration.y = float(calibrated_imu_data[1])
        self.imu_msg.linear_acceleration.z = float(calibrated_imu_data[2])
        self.imu_msg.angular_velocity.x = gyro_x
        self.imu_msg.angular_velocity.y = gyro_y
        self.imu_msg.angular_velocity.z = gyro_z
        self.pub_imu_calib.publish(self.imu_msg)
        rospy.loginfo("Publishing into /imu/calib_data topic!")
        
        				"""
				   SERVO CONTROL
					"""    
    def servo_angles_callback(self, data):
        if not self.pi.connected:
            rospy.logerror('pigpio not connected')
        elif data.angle1 > self.MAX_ANGLE or data.angle1 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')    ## do not display this error -> create an algorithm to compensate
        elif data.angle2 > self.MAX_ANGLE or data.angle2 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range') 
        elif data.angle3 > self.MAX_ANGLE or data.angle3 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')  
        elif data.angle4 > self.MAX_ANGLE or data.angle4 < self.MIN_ANGLE:
            rospy.logerror('Angle out of range')                  
        else:        
            y1 = self.MIN_PWM
            y2 = self.MAX_PWM
            x1 = self.MIN_ANGLE
            x2 = self.MAX_ANGLE
            pwm1 = (y2-y1)*(data.angle1 - x1)/(x2-x1) + y1
            pwm2 = (y2-y1)*(data.angle2 - x1)/(x2-x1) + y1
            pwm3 = (y2-y1)*(data.angle3 - x1)/(x2-x1) + y1
            pwm4 = (y2-y1)*(data.angle4 - x1)/(x2-x1) + y1
            self.pi.set_servo_pulsewidth(self.Servo1, pwm1)
            self.pi.set_servo_pulsewidth(self.Servo2, pwm2)
            self.pi.set_servo_pulsewidth(self.Servo3, pwm3)
            self.pi.set_servo_pulsewidth(self.Servo4, pwm4)
            self.servo_pwm_msg.pulsewidth1 = pwm1
            self.servo_pwm_msg.pulsewidth1 = pwm2
            self.servo_pwm_msg.pulsewidth1 = pwm3
            self.servo_pwm_msg.pulsewidth1 = pwm4
            self.publish_const_servo_pulsewidth()     
            rospy.loginfo("Publishing into /servo_pwm topic!")
            
    def publish_once_servo_pulsewidth(self):
        """
        This is because publishing in topics sometimes fails the first time you publish.
        In continuous publishing systems, this is no big deal, but in systems that publish only
        once, it IS very important.
        """
        while not self.ctrl_c:
            connections = self.pub_servo_pwm.get_num_connections()
            if connections > 0:
                self.pub_servo_pwm.publish(self.servo_pwm_msg)
                rospy.loginfo("Pulsewidth Published")
                break
            else:
                self.rate.sleep()
     
    def publish_const_servo_pulsewidth(self):
        while not self.ctrl_c:
                self.pub_servo_pwm.publish(self.servo_pwm_msg)
                rospy.loginfo("Publishing Pulsewidth")
                self.rate.sleep()     
        
    def shutdownhook(self):
        # works better than the rospy.is_shutdown()
        self.ctrl_c = True


    def stop_all_servos(self):
       rospy.loginfo('Stopping all of servos...')
       self.pi.set_servo_pulsewidth(Servo1, 0) 
       self.pi.set_servo_pulsewidth(Servo2, 0) 
       self.pi.set_servo_pulsewidth(Servo3, 0) 
       self.pi.set_servo_pulsewidth(Servo4, 0) 
       
    def stop_one_servo(self, servo = 1):
       if servo == 18 or 1:
           self.pi.set_servo_pulsewidth(Servo1, 0) 
       elif servo == 12 or 2:
           self.pi.set_servo_pulsewidth(Servo1, 0)        
       elif servo == 18 or 3:
           self.pi.set_servo_pulsewidth(Servo1, 0) 
       elif servo == 12 or 4:
           self.pi.set_servo_pulsewidth(Servo1, 0)          
       else:     
           rospy.loginfo("Servo is incorrectly defined")
       rospy.loginfo("Stopping servo %d", servo)        
       

if __name__ == '__main__':
    rospy.init_node('imu_node')
    obj = SensorPub()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        obj.pub_acc_gyro()
        obj.pub_magnetometer()
        obj.calibrated_mag_data()
        obj.calibrated_imu_data()
        rate.sleep()

