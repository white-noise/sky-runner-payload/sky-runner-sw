_[TOC]_


<img src="skyrunner.png" height=250 />



- [ ] Software and control algorithms are developed and structured as _ROS packages_. 
- [ ] We started developing _ROS packages_ using _Python_.
- [ ] Currently we are migrating to C++


## Hardware used for _wnoise ROS package_
- Raspberry Pi Zero 2W
- 9-DOF Imu (FXOS8700 + FXAS21002C)
- BME280 
- PCA9685 (Currently migrating to SW PWM control of the servo)
- 4 micro servos FS90
- GPS module (using SPI to communicate with the R-Pi)
- Telemetry (XBee S2C pro, using SPI to communicate with the R-Pi)


## How to install ROS on Ubuntu and Raspberry Pi Zero 2W:
Follow the steps [here](https://gitlab.com/white-noise/sky-runner-payload/sky-runner-sw/-/wikis/Ubuntu-setup-and-ROS-installation-on-RPi-Zero-2W-Guide).

## Install _wnoise ROS package_ dependancies:

### Update Your Pi and Python

Run the standard updates:

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python3-pip
```
and
```
sudo pip3 install --upgrade setuptools
```

### Enable I2C and SPI:
```
cd ~
sudo pip3 install --upgrade adafruit-python-shell
wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
sudo python3 raspi-blinka.py
```
### Python Installation of FXOS8700 + FXAS21002C Library

```
sudo apt-get install -y python-rospy
sudo pip3 install Adafruit-Blinka
sudo pip3 install adafruit-circuitpython-fxos8700
sudo pip3 install adafruit-circuitpython-fxas21002c
```



## How to compile _wnoise ROS package_

How to create and compile packages is described [here](https://gitlab.com/white-noise/sky-runner-payload/sky-runner-sw/-/wikis/ROS-Fundamentals-Guide).
> Warning! You should copy the **wnoise** directory into `~/catkin_ws/src`. 

> DON'T FORGET to compile packages in **~/catkin_ws** directory and to run `source devel/setup.bash` after every compilation to update the packages. 

> If you get this error `[rospack] Error: package 'wnoise' not found` after running `catkin_make` try running `source devel/setup.bash` while you are in the **~/catkin_ws** directory.

## Get IMU data

```
rosrun wnoise get_imu_data.py
```

To change the rate you are getting the infrmation modify **line 55**:
```
rate = rospy.Rate(10) #the frequency is defined in Hz
```

