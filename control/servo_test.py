import time
from board import SCL, SDA
import busio
from adafruit_pca9685 import PCA9685
from adafruit_motor import servo

i2c = busio.I2C(SCL, SDA)

pca = PCA9685(i2c)
pca.frequency = 50

servo1 = servo.Servo(pca.channels[1], min_pulse=1230, max_pulse=2070)
servo8 = servo.Servo(pca.channels[8], min_pulse=1170, max_pulse=1900)
servo11 = servo.Servo(pca.channels[11], min_pulse=1000, max_pulse=1820)
servo6 = servo.Servo(pca.channels[6], min_pulse=1050, max_pulse=1850)





for i in range(20):
    servo1.angle = 0
    servo8.angle = 0
    servo11.angle = 0
    servo6.angle = 0
    time.sleep(0.5)

    servo1.angle = 90
    servo8.angle = 90
    servo11.angle = 90
    servo6.angle = 90
    time.sleep(0.5)

    servo1.angle = 90
    servo8.angle = 90
    servo11.angle =90
    servo6.angle = 90
    time.sleep(0.5)

    servo1.angle = 90
    servo8.angle = 90
    servo11.angle = 90
    servo6.angle = 90
    time.sleep(0.5)



pca.deinit()    
