function dx = x_dot(x,m,I,Fa,Fr,Ta,Tr)
%% %This is the handle function that will be used by ode45 function for integration
%of the ODE's of our dynamical model. 
%It describes the dynamics of the system in the body frame in form of differential equations.

% phi, theta, psi - Euler Angles
% U, V, W - linear velocities in x,y and z direction accordingly defined in
% body frame
% P, Q, R - angular velocities in x,y and z direction accordingly defined in
% body frame
% x = [phi theta psi U V W P Q R] - the new state vector
% dx = [phi_dot theta_dot psi_dot U_dot V_dot W_dot P_dot Q_dot R_dot]


% Fa = [Xa Ya Za] %aerodynamic forces
% Fr = [Xr Yr Zr] %force induced by the rotor
% Ta = [La Ma Na] %aerodynamic torques
% Tr = [Lr Mr Nr] %torque induced by the rotor
% I = [Ixx Iyy Izz] - moments of inertia 
% Omega - spin rate of the autogyro rotor in steady state

% closed-loop control equation :
% d/dt(x) = A*x +Bu
  
phi = x(1);
theta = x(2);
%psi = x(3); 
U = x(4); 
V = x(5); 
W = x(6); 
P = x(7); 
Q = x(8); 
R = x(9);

Fr(3) = 0.5*1.2*0.85*pi*(10e-3)^2*Wa^2;
%C = body2local_frm_matrix(phi,theta,psi);

dx(1,1) = P + (Q*sin(phi)+R*cos(phi))*tan(theta);
dx(2,1) = Q*cos(phi) -R*sin(phi);
dx(3,1) = (Q*sin(phi)+R*cos(phi))*sec(theta);
dx(4,1) = Fa(1)/m + Fr(1)/m - sin(theta)*g + R*V -Q*W;
dx(5,1) = Fa(2)/m + Fr(2)/m  + cos(theta)*sin(phi)*g + P*W -R*U;
dx(6,1) = Fa(3)/m + Fr(3)/m  + cos(phi)*cos(theta)*g +Q*U -P*V;
dx(7,1) = 1/I(1)*(Ta(1) + Tr(1) + Q*R*(I(2) -I(3)));
dx(8,1) = 1/I(2)*(Ta(2) + Tr(2) + R*P*(I(3) - I(1)));
dx(9,1) = 1/I(3)*(Ta(3) + Tr(3) + P*Q*(I(1)-I(2)));
end

