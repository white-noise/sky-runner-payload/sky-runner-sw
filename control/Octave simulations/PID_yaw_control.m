%% Parameters 
m = 0.123;
r = 0.218;
M = 0.415;
R = 0.05;

H = 0.08;
x_M = H/2;
x_m = 0.155;
x_cm = (m*x_m+M*x_M)/(m+M);

Ix = 1/4*m*r^2 + 1/4*M*R^2 + 1/12*M*H^2 + M*(x_M -x_cm)^2 + m*(x_m -x_cm)^2;
Iy = Ix;
Iz_all = 1/2*m*r^2 + 1/2*M*R^2; 
Iz = Iz_all - 1/2*m*r^2; % frictionless bearing assumtion

%% Continuous Time Model definition

A = [ 0 1; 
      0 0;  
      ];
B = [0;
     1/Iz;];
C = eye(2, 2);
D = zeros(2, 1);


%% State-space Model Definition
states = {'phi', 'theta', 'psi','phi_{dot}', 'theta_{dot}', 'psi_{dot}'};
inputs = {'M_x' 'M_y' 'M_z'};

% State-Space Model
SS = ss(A, B, C, D, 'statename',states,'inputname',inputs,'outputname',states);
% Transfer functions
G = tf(SS);


%% Open-Loop Step Response

s = tf('s');
P = 1  / (s^2 * Iz); 
step(P)

%%PID controller

Kp = 1;
Ki = 1;
Kd = 1;
Controller = pid(Kp,Ki,Kd);
T = feedback(Controller*P,1);

t = 0:0.01:2;
step(T,t)





% s = tf('s'); 
% % Yaw transfer function  psi_ddot = Mz/Iz 
% G = 1  / (s^2 * Iz);
% Kp = 0.00324;
% Kd = 0.0028;
% Ki = 
% C = Kp + Kd*s ; 
% K = [Kp Kd];
% Loop = series(C,G); % K - controller, G - system, with this command I build a series of transfer fcns K->G
% %(or I could multiply them), This is the open-loop system
% ClosedLoop = feedback(Loop,1); %This  adds feedback to open-loop created above
% 
% SScl = ss(ClosedLoop); % Convert to State-Space Model
% 
% % Simulate SS Model
% t = 0 : 0.001 : 10;
% r = zeros(1, length(t));
% X0 = [0.1; 0;];
% [y,t,x]=lsim(SScl,r',t, X0);
% 
% u_control = - (K * x')'; % Feedback Control Law u = - K x
% 
% 
% %% Plot Results
% figure;
% hold on
% [AX,~,~] = plotyy(t,x(:, 1),t,x(:, 2),'plot');
% 
% title('State Vectors')
% set(get(AX(1),'Ylabel'),'String','phi_z / psi (rad)')
% set(get(AX(2),'Ylabel'),'String','dot phi_z / dot psi (rad)')
% xlabel('Time (s)')
% 
% hold off
% 
% figure;
% hold on
% plot(t, u_control,LineWidth=10);
% title('Control Input z-Moment (Nm)')
% ylabel('Mz (Nm)')
% xlabel('Time (s)')
% hold off
% 
% 

