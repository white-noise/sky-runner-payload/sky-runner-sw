function C = body2local_frm_matrix(phi,theta,psi)
%C -body to local frame transformation matrix 
%phi,theta,psi - Euler angles
Cxx = cos(psi)*cos(theta);
Cxy = cos(psi)*sin(theta)*sin(phi) - sin(psi)*cos(phi);
Cxz = cos(psi)*sin(theta)*cos(phi) + sin(psi)*sin(phi);
Cyx = sin(psi)*cos(theta);
Cyy = sin(psi)*sin(theta)*sin(phi) + cos(psi)*cos(phi);
Cyz = sin(psi)*sin(theta)*cos(phi) - cos(pi)*sin(phi);
Czx = -sin(theta);
Czy = cos(theta)*sin(phi);
Czz = cos(theta)*cos(phi);
C = [Cxx Cxy Cxz; Cyx Cyy Cyz; Czx Czy Czz];
end