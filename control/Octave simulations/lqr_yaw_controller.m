%% Parameters 
m = 0.123;
r = 0.218;
M = 0.415;
R = 0.05;

H = 0.08;
x_M = H/2;
x_m = 0.155;
x_cm = (m*x_m+M*x_M)/(m+M);

Ix = 1/4*m*r^2 + 1/4*M*R^2 + 1/12*M*H^2 + M*(x_M -x_cm)^2 + m*(x_m -x_cm)^2;
Iy = Ix;
Iz_all = 1/2*m*r^2 + 1/2*M*R^2; 
Iz = Iz_all - 1/2*m*r^2; % frictionless bearing assumtion

%% Continuous Time Model definition

A = [ 0 1; 
      0 0;  
      ];
B = [0;
     1/Iz;];
C = eye(2, 2);
D = zeros(2, 1);
%% Controllability and observability 

controllability = rank(ctrb(A,B)); % rank = 2 is controllable
observability = rank(obsv(A,C)); % rank = 2 is observable

%% State-space Model Definition
states = {'psi', 'psi_{dot}'};
inputs = {'M_z'};

% State-Space Model
SS = ss(A, B, C, D, 'statename',states,'inputname',inputs,'outputname',states);
% Transfer functions
G = tf(SS);



% Maximum actuation 
M_z_max = 0.0136;


phi_z_max = 20 * pi / 180;
 
%% LQR

Q = [10     0
     0     10]; 
% The higher the value the more it is penalized

R = 10000000/5.5;
% The smaller the value the more aggressive the control


[K, P, e] = lqr(A, B, Q, R);
 
 
% Controlled system
 
Ac = A - B * K;
Bc = zeros(2, 1);
 
 
 SScl = ss(Ac, Bc, C, D,'statename',states,'inputname',inputs,'outputname',states);
 
 t = 0 : 0.001 : 10;
 r = pi*ones(1, length(t));
 X0 =  [pi; pi];
 figure(1);
lsim(SScl,r',t, X0)
grid on 
[y,t,x]=lsim(SScl,r',t, X0);
% 
% Feedback Control Law u = - K x
 u_control =  -(K * x')';
%  
 % Moment
figure(2);
%plot(t,(I_bz-Iy) * u_control(:,1),t,(I_bz-Iy) * u_control(:,2),t,(I_bz-Iy) * u_control(:,3));
plot(t, u_control);

title('Inputs')

% max u should be 0.0162 Nm 