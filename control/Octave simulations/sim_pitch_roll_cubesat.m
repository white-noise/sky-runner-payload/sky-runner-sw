%% Simulate the dynamics (mathematical) model of the cubecat 
%(simplified:only roll and pitch taken into account)

clear all, close all, clc
%% Model Parameters 
m = 75e-3;
r = 0.35;
M = 0.33;
%mu = (M * m) / (M + m);
R = 0.05;
H = 0.3;
h = 0.15;
rho = h * m / (M + m);
rho_h = h * M / (M + m);
%Moments of Inertia
Ix = 1 / 4 * m * r^2 + 1 / 12 * M * (3 * R^2 + H^2) + M * rho_h ^ 2 + m * rho^2;
Iy = Ix;
Iz = 1 / 2 * m * r^2; 
My = 0;
% Autogyro Rotor Rotation Rate (steady state)
Omega_rpm  = 1790.3; % speed of gyro (rpm)
Omega = pi / 30 * Omega_rpm;
%% Integration         
tspan = 0:.1:50;
x0 = [0; 0; pi; .5];
[t,x] = ode45(@(t,x)pitch_roll_cubesat(x,Ix,Iy,Iz,5*sin(t) + 3*t,My,Omega),tspan,x0);
%% Uncomments to visualize the plots (while commenting the animation)
% figure(1) % phi vs time
% plot(t,x(:,1),'-') 
% figure(2) % phi_dot vs time
% plot(t,x(:,2),'-') 
% figure(3) % theta vs time
% plot(t,x(:,3),'-') 
% figure(4) % theta_dot vs time
% plot(t,x(:,4),'-') 
%% Comment these lines and uncomment the previous to see the plots
% Animation of roll of the cubesat 
% to change to animation of pitch of the cubesat edit draw_cubesat.m
 for k=1:length(t)
     draw_cubesat(x(k,:),[0 1 0]);
 end
%%
