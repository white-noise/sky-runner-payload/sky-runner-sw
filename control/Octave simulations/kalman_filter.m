%% Build SS Model 

% x = [psi, psi_dot]' - state vector 
% u = [T_s] - actuation 

% psi: yaw angle of the payload body 
% psi_dot: yaw rotational velocity 
% T_s: Moment applied in the direction of the z axis of the body

% open-loop control equation
% d/dt(x) = A*x +Bu

syms s b J

A = [0 1; 0 -b/J];
B = [0;1/J];
C = [1 1]; % measure yaw from magnetometer, yaw_dot from gyroscope
D = zeros(size(C,1),size(B,2));
%D = 0;

%Find the trunsfer function 
TF = C/(s*eye(size(A)) -A)*B + D';

%% Define SS Model
%s = tf('s');

%Define model parameters
b = 0.001; %Nms/rad
%b = 0;
J = 0.00153; %Nms^2/rad

%Define transfer function 
%t_f = (s + 1)/(s*(b + J*s));

A = [0 1; 0 -b/J];
B = [0;1/J];
C = [1 1]; % measure yaw from magnetometer, yaw_dot from gyroscope
D = zeros(size(C,1),size(B,2));

% Check controllability and obsrvability of the model 
rank(ctrb(A,B)); %If this is equal the number of system variables the system is controllable
rank(obsv(A,C)); %is observable if not 0 


%% Root locus 
%Find if the system is stable 
sys = tf([1 1],[J b 0]);
%rlocus(sys)
%grid on

%% Kalman Filter 

%  Specify disturbance and noise magnitude
Vd = 10*eye(2);  % disturbance covariance
Vn = 1000000;       % noise covariance

%  Build Kalman filter

%[Kf,P,E] = lqe(A,Vd,C,Vd,Vn);  % design Kalman filter(%here I assume that disturbance has no transfer function and its purely additive)
Kf = (lqr(A',C',Vd,Vn))';

%% Augment system with additional inputs
Baug = [B eye(2) 0*B];  % [u I*wd 0*wn]
Daug = [0 0 0 1];   % D matrix passes noise through

sysC = ss(A,Baug,C,Daug);  % single-measurement system 

% "true" system w/ full-state output, disturbance, no noise
sysTruth = ss(A,Baug,eye(2),zeros(2,size(Baug,2)));  

sysKF = ss(A-Kf*C,[B Kf],eye(2),0*[B Kf]);  % Kalman filter

%%  Estimate system 
dt = .01;
t = dt:dt:50;

uDIST = sqrt(Vd)*randn(2,size(t,2)); % random disturbance
uNOISE = sqrt(Vn)*randn(size(t));    % random noise
u = 0*t;
u(1/dt) = 20/dt;    % positive impulse
u(15/dt) = -20/dt;  % negative impulse

uAUG = [u; uDIST; uNOISE];  % input w/ disturbance and noise

[y,t] = lsim(sysC,uAUG,t);         % noisy measurement
[xtrue,t] = lsim(sysTruth,uAUG,t); % true state
[xhat,t] = lsim(sysKF,[u; y'],t);  % state estimate

%% Plot signals 

figure
plot(t,y,'Color',[.5 .5 .5])
hold on
plot(t,xtrue(:,1),'Color',[0 0 0],'LineWidth',2)
plot(t,xhat(:,1),'--','LineWidth',2)
set(gcf,'Position',[100 100 500 300])
xlabel('Time')
ylabel('Measurement')
l1 = legend('y (measured)','y (no noise)','y (KF estimate)');
grid on








